#include "escena.h"

Escena::Escena(){
    animacionActiva=0;
    dibujar_ejes=true;

    luz1.setPosicion(0,1,0,0);
    //Material de todos los elementos
    todo.set_difusa(1,0,0);
    todo.set_ambiental(0.7,0,0);
    todo.set_especular(0.0000000001,0.0000000001,0.0000000001);
    todo.set_brillo(6);
    //####################################################
    //EJES
    //####################################################
    float AXIS_SIZE=60;
    ejes=new float[18]{-AXIS_SIZE,0,0,
                      AXIS_SIZE,0,0,
                      0,-AXIS_SIZE,0,
                      0,AXIS_SIZE,0,
                      0,0,-AXIS_SIZE,
                      0,0,AXIS_SIZE,
                     };
    coloresEjes=new float[18]{1,0,0, 1,0,0,
                        0,1,0, 0,1,0,
                        0,0,1, 0,0,1
                     };
    //####################################################
    //CAMARAS
    //####################################################
    //cam-> indice de la camara activa
    cam=0;
    camaras.resize(1);
    camaras[0].moverCamara(0,0,300);

    float cub[]={0.0,-1.0,0.0,
                 0.7071067812,-1.0,0.0,
                 0.7071067812,0.0,0.0,
                 0.0,0.0,0.0};
    cubo.set(cub,sizeof(cub)/sizeof(float));
    cubo.generarFiguraRevolucion(4);
    cubo.rutaTextura("../sg-practica1/texturas/hierba.jpg");
}

void Escena::change_observer(){
    camaras[cam].cargarCamara();
}
void Escena::draw(){
    change_observer();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    if(dibujar_ejes)
        draw_axis();
    //todo.activar();
    //##################################################
    //SE DIBUJA EL CARRUSEL
    glInitNames();
    glPushName(0);
    //glScalef(5,5,5);
    glPushMatrix();
        glScalef(1.15,1.15,1.15);
        glTranslatef(15,0,-10);
        tiovivo.dibujar();
    glPopMatrix();
    glPopName();
    //##################################################
    //SE DIBUJA LA PISTA DE COCHES DE CHOQUE
    glPushName(1);
    glPushMatrix();
        glScalef(1.7,1.7,1.7);
        glTranslatef(-7,0,-10);
        glRotatef(90,0,1,0);
        pista.dibujar();
    glPopMatrix();
    glPopName();
    //##################################################
    //SE DIBUJA LA NORIA
    glPushName(2);
    glPushMatrix();
        glScalef(2.4,2.4,2.4);
        glTranslatef(-14,0,7);
        glRotatef(90,0,1,0);
        noria.dibujar();
    glPopMatrix();
    glPopName();
    //##################################################
    //SE DIBUJA EL SUELO CON UN CUBO
    glPushMatrix();
        glScalef(90,1,60);
        glTranslatef(-0.5,-0.5,0);
        glRotatef(90,0,0,1);
        glRotatef(45,0,1,0);
        cubo.dibujar();
    glPopMatrix();
}
void Escena::inicializar(){
    change_observer();
    glClearColor(1,1,1,1);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    //glLightModelfv(GL_LIGHT_MODEL_AMBIENT,Ambiente);
    //glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    luz1.enable();
    //todo.activar();
}
void Escena::dibujarEjes(bool valor){
    dibujar_ejes=valor;
}
void Escena::normal_keys(unsigned char Tecla,int x,int y){
    float veloCam=0.5;
    switch (toupper(Tecla)){
        case 'A':camaras[cam].moverVista(-veloCam,0,0);break;
        case 'D':camaras[cam].moverVista(veloCam,0,0);break;
        case 'W':camaras[cam].moverVista(0,veloCam,0);break;
        case 'S':camaras[cam].moverVista(0,-veloCam,0);break;
        case 'Z': animacionActiva=(animacionActiva+1)%2;
    }
}
void Escena::special_keys(int Tecla,int x,int y)
{
    switch (Tecla){
        case GLUT_KEY_LEFT: camaras[cam].rotarCamara(1,0);break;
        case GLUT_KEY_RIGHT: camaras[cam].rotarCamara(-1,0);break;
        case GLUT_KEY_UP: camaras[cam].rotarCamara(0,1);break;
        case GLUT_KEY_DOWN: camaras[cam].rotarCamara(0,-1);break;
        case GLUT_KEY_PAGE_UP: camaras[cam].moverCamara(0,0,20);break;
        case GLUT_KEY_PAGE_DOWN: camaras[cam].moverCamara(0,0,-20);
    }
}
void Escena::draw_axis(){
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3,GL_FLOAT,0,ejes);
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(3,GL_FLOAT,0,coloresEjes);
    glDrawArrays(GL_LINES,0,6);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
}
bool Escena::animacion(){
    if(animacionActiva){
        noria.animacion();
        tiovivo.animacion();
        pista.animacion();
        return true;
    }
    return false;
}
void Escena::seleccionar(int parte,int comp){
    switch (parte) {
    case 0:
        tiovivo.detener(comp);
        break;
    case 1:
        pista.detenerCoche(comp);
    }
}
