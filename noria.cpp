#include "noria.h"
#include <math.h>
Noria::Noria()
{
    //Numero de cabinas = 10
    nCab=10;
    //Rotacion de la noria inicial
    rot=0;
    //Este objeto sera los anillos de la noria
    float an[]={5.0,0.0,0.0, 5.3,0.0,0.0, 5.3,0.3,0.0, 5.0,0.3,0.0, 5.0,0.0,0.0};
    anillo.set(an,sizeof(an)/sizeof(float));
    anillo.generarFiguraRevolucion(100);
    Material azul;
    azul.set_ambiental(0.0,0.1,0.2);
    azul.set_difusa(0.0,0.50980392,0.50980392);
    azul.set_especular(0.20196078,0.30196078,0.80196078);
    azul.set_brillo(1);
    anillo.setMaterial(azul);
    //Cilindros que se dibujaran sosteniendo los anillos
    float cil[]={0.0,0.0,0.0,
                 0.7,0.0,0.0,
                 0.7,1.0,0.0,
                 0.0,1.0,0.0};
    cilindro.set(cil,sizeof(cil)/sizeof(float));
    cilindro.generarFiguraRevolucion(20);
    cilindro.setMaterial(azul);
}
/*********************************************************************
*Dibuja la pieza formada por un anillo y los cilindros
*********************************************************************/
void Noria::dibujarAnillo(){
    glPushMatrix();
        glRotatef(90,0,0,1);
        anillo.dibujar();
    glPopMatrix();
    glPushMatrix();
        glTranslatef(-0.15,-5,0);
        glScalef(0.2,10,0.2);
        cilindro.dibujar();
    glPopMatrix();
    glPushMatrix();
        glRotatef(90,1,0,0);
        glTranslatef(-0.15,-5,0);
        glScalef(0.2,10,0.2);
        cilindro.dibujar();
    glPopMatrix();
}
void Noria::dibujar(){
    glPushMatrix();
        glTranslatef(0,7,0);
        glRotatef(rot,1,0,0);
        //Se dibuja el cilindro central
        glPushMatrix();
            glTranslatef(1.5,0,0);
            glScalef(3,1,1);
            glRotatef(90,0,0,1);
            cilindro.dibujar();
        glPopMatrix();

        //Dibujamos los dos anillos en su posicion
        glPushMatrix();
            glTranslatef(-0.6,0,0);
            dibujarAnillo();
            glTranslatef(1.5,0,0);
            dibujarAnillo();
        glPopMatrix();
        //Se dibujan n cabinas
        float rad=M_PI*2/nCab;
        for(int i=0;i<nCab;i++){
            glPushMatrix();
                glTranslatef(0,sin(rad*i)*5.2,cos(rad*i)*5.2);
                glRotatef(-rot,1,0,0);
                //if(i==0)glGetFloatv(GL_MODELVIEW_MATRIX, matrizT);
                cabina.dibujar();
            glPopMatrix();
        }
    glPopMatrix();
    //Soportes de la noria
    glPushMatrix();
        glTranslatef(-0.85,0,0);
        glScalef(1.17,1.17,1.17);
        soporte.dibujar();
    glPopMatrix();
    glPushMatrix();
        glRotatef(180,0,1,0);
        glTranslatef(-0.85,0,0);
        glScalef(1.17,1.17,1.17);
        soporte.dibujar();
    glPopMatrix();
}
void Noria::animacion(){
    rot+=0.4;
}
void Noria::getPosCabina(_vertex3f &pos,_vertex3f &vista){
    /*
    _vertex3f aux(0,1,0);
    float xp = matrizT[0] * aux.x + matrizT[4] * aux.y + matrizT[8] * aux.z + matrizT[12];
    float yp = matrizT[1] * aux.x + matrizT[5] * aux.y + matrizT[9] * aux.z + matrizT[13];
    float zp = matrizT[2] * aux.x + matrizT[6] * aux.y + matrizT[10] * aux.z + matrizT[14];
    float wp = matrizT[3] * aux.x + matrizT[7] * aux.y + matrizT[11] * aux.z + matrizT[15];

    pos.x=xp/wp;
    pos.y=yp/wp;
    pos.z=zp/wp;
    pos.show_values();
    vista.x=xp/wp;
    vista.y=yp/wp;
    vista.z=2+zp/wp;
    */
}
