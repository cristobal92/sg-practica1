#ifndef ESCENA_H
#define ESCENA_H
#include "camara.h"
#include <GL/glut.h>
#include "objeto3dply.h"
#include "objeto3drev.h"
#include "luz.h"
#include "pistaChoque.h"
#include "noria.h"
#include "carrusel.h"
#include "material.h"
class Escena{
private:
    vector<Camara> camaras;
    Luz luz1;
    Material todo;
    int cam,animacionActiva;
    //Cubo usara para el suelo
    Objeto3DRev cubo;
    bool dibujar_ejes;
    void draw_axis();
    float AXIS_SIZE;
    float *ejes,*coloresEjes;
    Carrusel tiovivo;
    pistaChoque pista;
    Noria noria;
public:
    Escena();
    void change_observer();
    void inicializar();
    void draw();
    void dibujarEjes(bool valor);
    void normal_keys(unsigned char Tecla,int x,int y);
    void special_keys(int Tecla,int x,int y);
    //Realiza la animacion y devuelve true para indicar si esta activa
    bool animacion();
    void seleccionar(int parte,int comp);
};

#endif // ESCENA_H
