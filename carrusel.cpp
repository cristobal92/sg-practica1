#include "carrusel.h"
#include <math.h>
Carrusel::Carrusel()
{
    animacionActiva=1;
    caballoDetenido=-1;
    //Numero de caballos
    nCaballos=4;
    //elevacion inicial de los caballos, pares a -0.3 e impares a 0.3
    elevCaballos[0]=-0.3;
    elevCaballos[1]=0.3;
    //Se usa para controlar el cambio de sentido de los caballos
    elevCaballos[2]=1;
    //Techo del carrusel
    float tec[]={0.0,1.0,0.0,
               1.0,1.0,0.0,
               5.5,0.0,0.0,
               5.5,1.0,0.0,
               5.4,1.0,0.0,
               5.4,0.1,0.0,
               0.0,2.4,0.0};
    techo.set(tec,sizeof(tec)/sizeof(float));
    techo.generarFiguraRevolucion(200);
    techo.rutaTextura("../sg-practica1/texturas/tiovivo.jpg");
    //Parte central del carrusel
    float cent[]={0.0,0.0,0.0,
               1.0,0.0,0.0,
               1.0,4.5,0.0,
               0.0,4.5,0.0};
    centro.set(cent,sizeof(cent)/sizeof(float));
    centro.generarFiguraRevolucion(7);
    centro.rutaTextura("../sg-practica1/texturas/tiovivo.jpg");
    //Codigo para la base del carrusel
    float b1[]={0.0,0.0,0.0,
               6.0,0.0,0.0,
               6.0,0.2,0.0,
               5.8,0.2,0.0,
               5.8,0.4,0.0,
               0.0,0.4,0.0};
    base1.set(b1,sizeof(b1)/sizeof(float));
    base1.generarFiguraRevolucion(200);
    base1.rutaTextura("../sg-practica1/texturas/madera.jpg");
    //Base superior que gira con el carrusel
    float b2[]={0.0,0.0,0.0,
                5.5,0.0,0.0,
                5.5,0.4,0.0,
                0.0,0.4,0.0};
    base2.set(b2,sizeof(b2)/sizeof(float));
    base2.generarFiguraRevolucion(200);
    //Tendra textura de madera
    base2.rutaTextura("../sg-practica1/texturas/tiovivo.jpg");
}
/*********************************************************************
*Funcion que dibuja cada componente en su posicion
*********************************************************************/
void Carrusel::dibujar(){
    glPushName(5);
    //Se dibuja la primera base, esta no se mueve
    base1.dibujar();
    glPushMatrix();
    //el resto del tiovivo gira
        glRotatef(rot,0,1,0);
        glTranslatef(0,0.4,0.0);
        base2.dibujar();
        glTranslatef(0,0.4,0.0);
        centro.dibujar();
        float rad=2*M_PI/nCaballos;
        float ang=360/nCaballos;
        for(int i=0;i<nCaballos;i++){
            caballo.setElevacion(elevCaballos[i%2]);
            glLoadName(i);
            glPushMatrix();
                glTranslatef(cos(rad*i)*3.2,0, -sin(rad*i)*3.2);
                glRotatef(ang*i+90,0,1,0);
                caballo.dibujar();
            glPopMatrix();
        }
        glTranslatef(0,3.5,0.0);
        techo.dibujar();
    glPopMatrix();
    glPopName();
}
/*********************************************************************
*Control de la animacion
*********************************************************************/
void Carrusel::animacion(){
    if( animacionActiva ){
        float v=0.01;
        rot+=1;
        //Si se supera el limite se cambia el sentido cambiando el signo de elevCaballos[2]
        if(elevCaballos[0]>0.4)
            elevCaballos[2]=-1;
        if(elevCaballos[0]<=-0.4)
            elevCaballos[2]=1;
        elevCaballos[0]+=v*elevCaballos[2];
        elevCaballos[1]-=v*elevCaballos[2];
    }

}
/*********************************************************************
*Detiene el carrusel
*********************************************************************/
void Carrusel::detener(int id){
    if(id==5){
        animacionActiva=(animacionActiva+1)%2;
    }
}
