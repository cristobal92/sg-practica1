#include "objeto3drev.h"
#include <math.h>
#include "iostream"
#include <assert.h>


Objeto3DRev::Objeto3DRev(){
    textura=0;
    Puntoinf=false,Puntosup=false;
}

Objeto3DRev::Objeto3DRev (float *V, int tamV, int *C, int tamC){
    Vertices.resize(tamV/3);
    for(int i=0,j=0;i<tamV;i+=3,j++){
        Vertices[j].x=V[i];
        Vertices[j].y=V[i+1];
        Vertices[j].z=V[i+2];
    }
    if(C!=0){
        Caras.resize(tamC);
        for(int i=0;i<tamC;i++){
            Caras[i]=C[i];
        }
    }
    else
        Caras.resize(0);
   textura=0;
}
/*********************************************************************
*Con se se introduce un perfil y se geenra una figura por revolucion
*********************************************************************/
void Objeto3DRev::generarFiguraRevolucion(int per){
    //Comprobamos que sea un perfil en el plano formado por X e Y y en el lado positivo de X
    for(unsigned int i=0;i<Vertices.size();i++)
        assert(Vertices[i].z==0 && Vertices[i].x>=0);

    int P;
    //Creamos los nuevos vertices
    /*Los vertices en los extremos del vector Vertices pueden ser parte de las tapas
     *por lo que se comprueba si estan en el eje y.
    */
    Puntoinf= Vertices.front().x==0;
    Puntosup= Vertices.back().x==0;

    float rad,angulo=(2*M_PI)/per;
    p=Vertices.size();
    Vertices.resize((per+1)*p);
    for(int i=1;i<=per;i++){
        rad=angulo*i;
        float coseno=cos(rad);
        float seno=sin(rad);
        for(int j=0;j<p;j++)
            Vertices[i*p+j]=_vertex3f(Vertices[j].x*coseno ,Vertices[j].y ,-Vertices[j].x*seno);

    }
    P=Vertices.size();
    //-------------------------------------------------------------------------------
    //GENERAMOS LAS CARAS
    int ini=0,fin=p-1;
    Caras.resize(0);
    Caras.reserve(p*per*3);
    //Si hay un punto de la tapa inferior(x=0) se generan a parte sus caras
    if(Puntoinf){
        //ini se incrementa para que en el otro bucle se comienze a partir de la segunda fila de la malla
        ini++;
        for(int i=0;Puntoinf && i<per;i++){
            Caras.push_back(( (i+1)*p   )%P);
            Caras.push_back(( 1+(i+1)*p )%P);
            Caras.push_back(( 1+i*p   )%P);
        }
    }
    //Si hay un punto de la tapa superior (x=0) se evita que genere las caras de la ultima fila decrementando fin
    if(Puntosup)fin--;
    for(int i=ini;i<fin;i++){
        for(int j=0;j<per;j++){
            Caras.push_back(( i+j*p     )%P);
            Caras.push_back(( i+(j+1)*p )%P);
            Caras.push_back(( (i+1)+j*p )%P);

            Caras.push_back(( i+(j+1)*p   )%P);
            Caras.push_back(( i+1+(j+1)*p )%P);
            Caras.push_back(( (i+1)+j*p   )%P);
        }
    }
    //Si hay un punto de la tapa superior (x=0) se generan a parte sus caras
    if(Puntosup){
        for(int i=0;i<=per;i++){
            Caras.push_back(( fin+i*p     )%P);
            Caras.push_back(( fin+(i+1)*p )%P);
            Caras.push_back(( (fin+1)+i*p )%P);
        }
    }

    generarNormales();
    generarCoordenadasT();
}
/*********************************************************************
*Calcular las normales de una figura generada por revolucion
*********************************************************************/
void Objeto3DRev::generarNormales()
{
    int tam=Caras.size();
    normalesC.resize(tam/3);
    normalesV.resize(Vertices.size());
    //Se generan las normales de las caras
    for(int i=0,j=0;i<tam;i+=3,j++){
        int a=Caras[i], b=Caras[i+1], c=Caras[i+2];
        _vertex3f AB=Vertices[b] - Vertices[a];
        _vertex3f BC=Vertices[c] - Vertices[b];
        _vertex3f ABC=AB.cross_product(BC);
        /*El vector normal del triangulo se suma en el indice correspondiente del vector
         *normalesV para realizar el calculo posteriormente de los vectores normales a los vertices
         */
        normalesV[a]+=ABC;
        normalesV[b]+=ABC;
        normalesV[c]+=ABC;
        ABC.normalize();
        normalesC[j]=ABC;
    }
    /*Como hay vertices en las mismas coordenadas como los del primer y ultimo perfil y
     *los de los eje Y se tienen que sumar los valores de normalesV correspondientes a esos
     *vertices. Primero se realizan los vertices del eje Y en los extremos de cada perfil y
     *despues los vertices del primer y segundo perfil exceptuando los extremos si estos ya se han hecho.
     *Por ultimo se normalizan todos los vectores para obtener las normales a los vertices
     */
    int ini=normalesV.size()-p,ini2=0;
    int fin=normalesV.size();
    /*Si hay varios puntos inferiores y=0 y en las mismas coordenadas (tapa) se hace una sumatoria
     *de los vectores normales y todos se igualan al resultado (vaux)
     */
    if(Puntoinf){
        ini++;
        ini2++;
        _vertex3f vaux(0,0,0);
        for(unsigned int i=0;i<Vertices.size();i+=p)
            vaux+=normalesV[i];
        for(unsigned int i=0;i<Vertices.size();i+=p)
            normalesV[i]=vaux;//normalesV[i].show_values();
    }
    /*Si hay varios puntos inferiores y=0 y en las mismas coordenadas (tapa) se hace una sumatoria
     *de los vectores normales y todos se igualan al resultado (vaux)
     */
    if(Puntosup){
        _vertex3f vaux(0,0,0);
        for(unsigned int i=p-1;i<Vertices.size();i+=p)
            vaux+=normalesV[i];
        for(unsigned int i=p-1;i<Vertices.size();i+=p)
            normalesV[i]=vaux;
    }
    //Se suman las normales del primer y ultimo perfil
    for(unsigned int i=ini,j=ini2;i<fin;i++,j++){
        normalesV[i]+=normalesV[j];
        normalesV[j]=normalesV[i];
    }
    //Se normalizan los vectores para completar el calculo de las normales de los vertices
    for(unsigned int i=0;i<normalesV.size();i++)
       normalesV[i].normalize();
}
/*********************************************************************
*Generar coordenadas de textura de una figura generada por revolucion
*********************************************************************/
void Objeto3DRev::generarCoordenadasT(){
    int nvertices=Vertices.size();
    int nperfiles=nvertices/p;
    coorTextura.resize(nvertices);
    //Calculo de las coordenadas de textura (X)
    //for(int i=nperfiles-1,t=0;i>=0;i--,t++){
    for(int i=0;i<nperfiles;i++){
        float k=(float)i/(nperfiles-1);
        for(int j=0;j<p;j++)
            coorTextura[i*p+j].x=k;
    }
    /*Se calculan las distancias y se guardan en aux
     *En la primera prueba la textura aparecia al reves por lo que las distancias se calculan partiendo del ultimo
     *punto del perfil(p-1)
     */
    vector<float> aux;
    aux.resize(p);
    aux[p-1]=0;
    for(int i=p-2;i>=0;i--){
        aux[i]=aux[i+1]+ (Vertices[i]-Vertices[i+1]).module();
    }
    //Calculo de las coordenadas de textura (Y)
    for(int i=0;i<nperfiles;i++){
        for(int j=0;j<p;j++)
            coorTextura[i*p+j].y=aux[j]/aux[0];
    }
}
/*********************************************************
*Añadir una textura al objeto
*********************************************************/
void Objeto3DRev::rutaTextura(char *ruta){
    textura=new Textura(ruta);
}
/*********************************************************
*Funcion de dibujado para iluminacion
*********************************************************/
void Objeto3DRev::dibujar(){
    if(textura){
        glEnable(GL_TEXTURE_2D);
        textura->activar();
    }
    if(material)
        material->activar();
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, &Vertices[0]);
    glNormalPointer(GL_FLOAT, 0, &normalesV[0]);
    glTexCoordPointer(2,GL_FLOAT,0,&coorTextura[0]);

    glDrawElements( GL_TRIANGLES, Caras.size(), GL_UNSIGNED_INT,&Caras[0] );

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
    glDisable(GL_TEXTURE_2D);
    if(material)
        material->desactivar();
}

