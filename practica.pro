HEADERS = \
    escena.h \
    camara.h \
    luz.h \
    material.h \
    textura.h \
    objeto3d.h \
    objeto3drev.h \
    objeto3dply.h \
    caballito.h \
    carrusel.h \
    pistaChoque.h \
    cabina.h \
    coche.h \
    noria.h \
    soportenoria.h

SOURCES = \
    jpg/jpg_imagen.cpp  \
    jpg/jpg_memsrc.cpp \
    jpg/jpg_readwrite.cpp \
    escena.cpp \
    practica.cpp \
    camara.cpp \
    luz.cpp \
    material.cpp \
    textura.cpp \
    objeto3d.cpp \
    objeto3drev.cpp \
    objeto3dply.cpp \
    caballito.cpp \
    carrusel.cpp \
    pistaChoque.cpp \
    cabina.cpp \
    coche.cpp \
    noria.cpp \
    soportenoria.cpp
LIBS += -lglut -lGLU -ljpeg

CONFIG += console
QT += opengl

