#include "luz.h"
#include <math.h>

Luz::Luz(GLenum l,GLfloat x,GLfloat y, GLfloat z, GLfloat w){
    nluz=l;
    setPosicion(x,y,z,w);
    setAmbiental();
    setDifusa();
    setEspecular();
}
void Luz::setPosicion(GLfloat x, GLfloat y, GLfloat z, GLfloat w){
    posicion[0]=x;
    posicion[1]=y;
    posicion[2]=z;
    posicion[3]=w;
}
void Luz::enable(){

    glLightfv(nluz, GL_AMBIENT, colorA);
    glLightfv(nluz, GL_DIFFUSE, colorD);
    glLightfv(nluz, GL_SPECULAR, colorS);
    glLightfv(nluz,GL_POSITION,posicion);
    glEnable(nluz);

}
void Luz::disable(){
    glDisable(nluz);
}
void Luz::setAmbiental(float r,float g,float b,float a){
    colorA[0]=r;
    colorA[1]=g;
    colorA[2]=b;
    colorA[3]=a;
}
void Luz::setDifusa(float r,float g,float b,float a){
    colorD[0]=r;
    colorD[1]=g;
    colorD[2]=b;
    colorD[3]=a;
}
void Luz::setEspecular(float r,float g,float b,float a){
    colorS[0]=r;
    colorS[1]=g;
    colorS[2]=b;
    colorS[3]=a;
}
