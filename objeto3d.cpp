#include "objeto3d.h"
#include "math.h"
#include "iostream"
#include <assert.h>

 /*********************************************************
 *Introducir el vector de vertices y el indice de caras
 *********************************************************/
void Objeto3D::set(float *V, int tamV, int *C, int tamC){
    Vertices.resize(tamV/3);
    for(int i=0,j=0;i<tamV;i+=3,j++){
        Vertices[j].x=V[i];
        Vertices[j].y=V[i+1];
        Vertices[j].z=V[i+2];
    }
    if(C!=0){
        Caras.resize(tamC);
        for(int i=0;i<tamC;i++){
            Caras[i]=C[i];
        }
    }
    else
        Caras.resize(0);
}
/*********************************************************
*Funcion para obtener el centro del objeto
*********************************************************/
void Objeto3D::centro(float *ver){
    if(!centroF){
        _vertex3f aux(0,0,0);
        for(int i=0;i<Vertices.size();i++){
            aux+=Vertices[i];
        }
        aux/=Vertices.size();
        centroF=new _vertex3f(aux);
    }
    if(ver!=0){
        ver[0]=centroF->x;
        ver[1]=centroF->y;
        ver[2]=centroF->z;
    }
}
/*********************************************************
*Funcion para dibujar en ajedrez
*********************************************************/
void Objeto3D::dibujarAjedrez(){
   if(colorGen){
        Colores.reserve(4*Caras.size()/3);
        for(int i=0;i<Caras.size()/9;i++){
            Colores.push_back(1);
            Colores.push_back(0);
            Colores.push_back(0);
            Colores.push_back(1);

            Colores.push_back(0);
            Colores.push_back(1);
            Colores.push_back(0);
            Colores.push_back(1);

            Colores.push_back(0);
            Colores.push_back(0);
            Colores.push_back(1);
            Colores.push_back(1);
        }
        colorGen=false;
    }
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &Vertices[0]);

    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, &Colores[0]);

    glDrawElements( GL_TRIANGLES, Caras.size(), GL_UNSIGNED_INT,&Caras[0] );

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
}
/*********************************************************
*Intoducir el material con el que se dibujara
*********************************************************/
void Objeto3D::setMaterial(Material mat){
    material=new Material(mat);
}

