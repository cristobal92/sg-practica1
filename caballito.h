#ifndef CABALLITO_H
#define CABALLITO_H
#include "objeto3dply.h"
#include "material.h"
class Caballito
{
private:
    Objeto3DPLY caballo,barra;
    Material matCaballo,matBarra;
    float elevacion;
public:
    Caballito();
    void dibujar();
    void setElevacion(float elev);
};

#endif // CABALLITO_H
