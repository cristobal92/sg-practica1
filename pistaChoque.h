//**************************************************************************
// Práctica SG
//
// Cristobal Molina Mallorquin y Rogelio Garcia Peña
//
//
//**************************************************************************

#ifndef PISTACHOQUE_H
#define PISTACHOQUE_H

#include "coche.h"
#include "objeto3drev.h"
#include <vector>
#include "vertex.h"

class pistaChoque 
{
	private:
    coche coches;
    Objeto3DRev base, parteMetal, techo,barra;
    Material metal;
    int rot;
    vector<_vertex3f> posCoches;
    vector<int> rotCoches,cochesDetenidos;
    float radioColision;
    bool colisiona(int c);
    void avanzar(int i);
	public:
	pistaChoque();
    void dibujar();
    void animacion();
    void detenerCoche(int id);
};


#endif
