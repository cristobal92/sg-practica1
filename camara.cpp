#include "camara.h"
#include <math.h>
Camara::Camara(float eX, float eY, float eZ, float cX, float cY, float cZ){
    center.x=cX;
    center.y=cY;
    center.z=cZ;
    eye.x=eX;
    eye.y=eY;
    eye.z=eZ;
}

void Camara::moverVista(float cX, float cY, float cZ){
    center.x+=cX;
    center.y+=cY;
    center.z+=cZ;
}


void Camara::moverCamara(float eX, float eY, float eZ){
    eye.x+=eX;
    eye.y+=eY;
    eye.z+=eZ;
}
void Camara::cargarCamara(){
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eye.x,eye.y,eye.z,center.x,center.y,center.z,0,1,0);
    glRotated(rotX,1,0,0);
    glRotated(rotY,0,1,0);
}
void Camara::rotarCamara(int roty, int rotx){
    rotX+=rotx;
    rotY+=roty;
}
