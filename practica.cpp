/*Teclas:
 *F2-> activar/desactivar animacion
 *t-> rotar entre los tipos ajedrez, aristas, puntos y solido
 *c-> Cañon
 *v-> Cabina
 *b-> Tanque
 *w,a,s,d -> movimiento
 *+/- -> aumentar/disminuir velocidad
 */
#include "stdlib.h"
#include "stdio.h"
#include <GL/glut.h>
#include <ctype.h>
#include <iostream>
#include "escena.h"
int animacionActiva=0;
Escena parque;
// variables que controlan la ventana y la transformacion de perspectiva
GLfloat Window_width,Window_height,Front_plane,Back_plane;
// variables que determninan la posicion y tamaño de la ventana X
int UI_window_pos_x=50,UI_window_pos_y=50,UI_window_width=1200,UI_window_height=700;
int estadoRaton[3], xc,yc,xp,yp;

//**************************************************************************
// Funcion para definir la transformación de proyeccion
//***************************************************************************

void change_projection()
{

glMatrixMode(GL_PROJECTION);
glLoadIdentity();

// formato(x_minimo,x_maximo, y_minimo, y_maximo,Front_plane, plano_traser)
//  Front_plane>0  Back_plane>PlanoDelantero)
glFrustum(-Window_width,Window_width,-Window_height,Window_height,Front_plane,Back_plane);
}


//**************************************************************************
//
//**************************************************************************

void draw_scene(void)
{
    parque.draw();
    glutSwapBuffers();
}



//***************************************************************************
// Funcion llamada cuando se produce un cambio en el tamaño de la ventana
//
// el evento manda a la funcion:
// nuevo ancho
// nuevo alto
//***************************************************************************

void change_window_size(int Ancho1,int Alto1)
{
    float ratio=Window_width/Window_height;
    change_projection();
    glViewport(0,0,Alto1*ratio,Alto1);
    glutPostRedisplay();
}


//***************************************************************************
// Funcion llamada cuando se produce aprieta una tecla normal
//
// el evento manda a la funcion:
// codigo de la tecla
// posicion x del raton
// posicion y del raton
//***************************************************************************

void normal_keys(unsigned char Tecla1,int x,int y)
{
    parque.normal_keys(Tecla1,x,y);
    glutPostRedisplay();
}

//***************************************************************************
// Funcion llamada cuando se produce aprieta una tecla especial
//
// el evento manda a la funcion:
// codigo de la tecla
// posicion x del raton
// posicion y del raton

//***************************************************************************

void special_keys(int Tecla1,int x,int y)
{
    parque.special_keys(Tecla1,x,y);
    glutPostRedisplay();
}



//***************************************************************************
// Funcion de incializacion
//***************************************************************************

void initialize(void)
{
    // se inicalizan la ventana y los planos de corte
    Window_height=2;
    //Window_width=2.85;
    Window_width=Window_height*((float)UI_window_width/(float)UI_window_height);
    Front_plane=30;
    Back_plane=10000;

    parque.inicializar();

    // se habilita el z-bufer
    glEnable(GL_DEPTH_TEST);
    change_projection();
    glViewport(0,0,UI_window_width,UI_window_height);
}

//***************************************************************************
// Funcion pick
//***************************************************************************
int pick( int x, int y, int * elemento, int * part )
{
    GLuint bufferSelec[40];
    GLint viewport[4];
    glSelectBuffer(40,bufferSelec);
    glGetIntegerv(GL_VIEWPORT,viewport);
    glRenderMode(GL_SELECT);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPickMatrix(x,(viewport[3] - y),3.0, 3.0, viewport);
    glFrustum(-Window_width,Window_width,-Window_height,Window_height,Front_plane,Back_plane);
    draw_scene();
    int hits= glRenderMode (GL_RENDER);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-Window_width,Window_width,-Window_height,Window_height,Front_plane,Back_plane);
    if(hits>0){
        //Obtenemos la id del objeto seleccionado
        int id=bufferSelec[3];
        //Obtenemos el nombre del elemento
        int parte=bufferSelec[bufferSelec[0]+2];
        //La z minima del elemento
        GLfloat Z=bufferSelec[1];
        int j=0;
        //Seleccionamos el elemento con la menor z minima
        for(int i=1;i<hits;i++){
            j+=bufferSelec[j]+3;
            GLfloat z=bufferSelec[j+1];
            if(z<=Z){
                Z=z;
                id=bufferSelec[j+3];
                parte=bufferSelec[bufferSelec[j]+j+2];
            }
        }
        *part=parte;
        *elemento=id;
        return 1;
    }
    else
        return 0;

}
void animacion(){
    //Solo se realiza la llamada glutPostRedisplay() cuando la animacion esta activa
    //parque.animacion();
    if(parque.animacion())
        glutPostRedisplay();
}
void clickRaton(int boton, int estado, int x, int y)
{
    estadoRaton[0]=estadoRaton[1]=estadoRaton[2]=0;
    if ( boton == GLUT_RIGHT_BUTTON ){
        if ( estado == GLUT_DOWN ){
            estadoRaton[2] = 1 ;
            xc = x ;
            yc = y ;
        }
    }
    if ( boton == GLUT_LEFT_BUTTON ){
        if ( estado == GLUT_DOWN ){
            xp = x ;
            yp = y ;
            int parte,id;
            if(pick(x,y,&id,&parte)){
                estadoRaton[1] = 1 ;
                parque.seleccionar(id,parte);
            }
        }
    }
}


//***************************************************************************
// Programa principal
//
// Se encarga de iniciar la ventana, asignar las funciones e comenzar el
// bucle de eventos//
//***************************************************************************

int main(int argc, char **argv)
{
    /**/// se llama a la inicialización de glut
    glutInit(&argc, argv);

    // se indica las caracteristicas que se desean para la visualización con OpenGL
    // Las posibilidades son:
    // GLUT_SIMPLE -> memoria de imagen simple
    // GLUT_DOUBLE6 -> memoria de imagen doble
    // GLUT_INDEX -> memoria de imagen con color indizado
    // GLUT_RGB -> memoria de imagen con componentes rojo, verde y azul para cada pixel
    // GLUT_RGBA -> memoria de imagen con componentes rojo, verde, azul y alfa para cada pixel
    // GLUT_DEPTH -> memoria de profundidad o z-bufer
    // GLUT_STENCIL -> memoria de estarcido
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    // posicion de la esquina inferior izquierdad de la ventana
    glutInitWindowPosition(UI_window_pos_x,UI_window_pos_y);

    // tamaño de la ventana (ancho y alto)
    glutInitWindowSize(UI_window_width,UI_window_height);

    // llamada para crear la ventana, indicando el titulo (no se visualiza hasta que se llama
    // al bucle de eventos)
    glutCreateWindow("Parque de atracciones");

    // asignación de la funcion llamada "dibujar" al evento de dibujo
    glutDisplayFunc(draw_scene);
    // asignación de la funcion llamada "cambiar_tamanio_ventana" al evento correspondiente
    glutReshapeFunc(change_window_size);
    // asignación de la funcion llamada "tecla_normal" al evento correspondiente
    glutKeyboardFunc(normal_keys);
    // asignación de la funcion llamada "tecla_Especial" al evento correspondiente
    glutSpecialFunc(special_keys);

    glutIdleFunc(animacion);
    glutMouseFunc( clickRaton );

    //glutMotionFunc( RatonMovido );

    // funcion de inicialización
    initialize();

    // inicio del bucle de eventos
    glutMainLoop();
    return 0;
}
