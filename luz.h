#ifndef LUZ_H
#define LUZ_H
#include "GL/glut.h"
class Luz
{
private:
    GLenum nluz;
    GLfloat posicion[4], colorA[4],colorD[4],colorS[4];
public:
    //0-> direccional
    Luz(GLenum l=GL_LIGHT0,GLfloat x=0,GLfloat y=0, GLfloat z=1, GLfloat w=0);
    void setPosicion(GLfloat x,GLfloat y, GLfloat z,GLfloat w);
    void setAmbiental(GLfloat r=1,GLfloat g=1,GLfloat b=1,GLfloat a=1);
    void setDifusa(GLfloat r=1,GLfloat g=1,GLfloat b=1,GLfloat a=1);
    void setEspecular(GLfloat r=1,GLfloat g=1,GLfloat b=1,GLfloat a=1);
    void enable();
    void disable();
};

#endif // LUZ_H
