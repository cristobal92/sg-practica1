#include "caballito.h"

Caballito::Caballito()
{
    caballo.setPLY("../sg-practica1/modelos/caballito_carrusel");
    barra.setPLY("../sg-practica1/modelos/barra_carrusel");
    Material oro;
    oro.set_ambiental(0.24725,0.1995,0.0745);
    oro.set_difusa(0.75164,0.60648,0.22648);
    oro.set_especular(0.000000000001,0.000000000001,0.000000000001);
    oro.set_brillo(1);
    barra.setMaterial(oro);
    caballo.setMaterial(oro);
    /*
    matBarra.set_ambiental(1,1,0.0);
    matBarra.set_difusa(1,1,0.0);
    matBarra.set_especular(0,0,0);
    matBarra.set_brillo(6);

    matCaballo.set_difusa(0.6,0,0);
    matCaballo.set_ambiental(0.6,0,0);
    matCaballo.set_especular(0.7,0.7,0.7);
    matCaballo.set_brillo(13);
    */
}
void Caballito::dibujar(){
    //Barra
    glPushMatrix();
        glTranslatef(0,1.55,0);
        glPushMatrix();
            glScalef(0.025,0.025,0.025);
            glRotatef(-90,1,0,0);
            barra.dibujar();
        glPopMatrix();
    //Caballo
        glPushMatrix();
            glTranslatef(0,elevacion,0);
            glScalef(0.019,0.019,0.019);
            glRotatef(90,0,1,0);
            glRotatef(-90,1,0,0);
            caballo.dibujar();
        glPopMatrix();
    glPopMatrix();
}
void Caballito::setElevacion(float elev){
    elevacion=elev;
}
