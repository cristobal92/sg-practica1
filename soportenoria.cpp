#include "soportenoria.h"

soporteNoria::soporteNoria()
{
    float cub[]={0.0,0.0,0.0,
                 0.7071067812,0.0,0.0,
                 0.7071067812,1.0,0.0,
                 0.0,1.0,0.0};
    cubo.set(cub,sizeof(cub)/sizeof(float));
    cubo.generarFiguraRevolucion(4);
    cubo.rutaTextura("../sg-practica1/texturas/basenoria.jpg");
    cilindro.set(cub,sizeof(cub)/sizeof(float));
    cilindro.generarFiguraRevolucion(20);
    cilindro.rutaTextura("../sg-practica1/texturas/metal.jpg");
}
void soporteNoria::dibujar(){
    glPushMatrix();
    glTranslatef(-1.75,0,0);
    //Los cilindros que sostienen la noria
    glPushMatrix();
        glTranslatef(0,0.2,1.5);
        glRotatef(-15,1,0,0);
        glRotatef(-15,0,0,1);
        glScalef(0.3,6,0.3);
        cilindro.dibujar();
    glPopMatrix();
    glPushMatrix();
        glTranslatef(0,0.2,-1.5);
        glRotatef(15,1,0,0);
        glRotatef(-15,0,0,1);
        glScalef(0.3,6,0.3);
        cilindro.dibujar();
    glPopMatrix();
    //La base, se dibuja a partir de un cubo
    glPushMatrix();
        glScalef(2,0.5,5);
        glTranslatef(0.3,0.2,0);
        glRotatef(90,0,0,1);
        glRotatef(45,0,1,0);
        cubo.dibujar();
    glPopMatrix();
    glPopMatrix();
}
