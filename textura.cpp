#include "textura.h"
#include "jpg/jpg_imagen.hpp"
#include <assert.h>
Textura::Textura(char *ruta){
    Id=-1;
    if(ruta)
        set_textura(ruta);
}

void Textura::set_textura(char *ruta)
{
    Id=-1;
    pimg= new jpg::Imagen(ruta);
    textX=pimg->tamX();
    textY=pimg->tamY();
    texels=pimg->leerPixels();
}
void Textura::activar()
{
    //Si no ha sido activada se obtiene un identificador y se carga la textura
    if(Id==-1){
        //Obtenemos una id para la textura
        glGenTextures(1,&Id);
        glBindTexture(GL_TEXTURE_2D,Id);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // parametros de aplicacion de la textura
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE);
        // asignacion de la textura
        glTexImage2D(GL_TEXTURE_2D,0,4,textX,textY,0,GL_RGB,GL_UNSIGNED_BYTE,texels);
    }
    glBindTexture(GL_TEXTURE_2D,Id);
}

