//**************************************************************************
// Práctica SG
//
// Cristobal Molina Mallorquin y Rogelio Garcia Peña
//
//
//**************************************************************************

#ifndef OBJETO3DREV_H
#define OBJETO3DREV_H


#include <GL/gl.h>
#include <vector>
#include "vertex.h"
#include "objeto3d.h"
#include "textura.h"
using namespace std;

class Objeto3DRev : virtual public Objeto3D
{
protected:
    /*Para los objetos que se generan por revolucion
     *Si hay alguno de los puntos de los estremos del perfil estan en el eje Y
     *las caras deben generarse de otra manera
     */
    bool Puntoinf,Puntosup;
    int p;
public:
    Objeto3DRev ();
    Objeto3DRev (float *V, int tamV, int *C=0, int tamC=0);
    void dibujar();
    void rutaTextura(char *ruta);
    void generarNormales();
    void generarFiguraRevolucion(int per);
    void generarCoordenadasT();
};
#endif
