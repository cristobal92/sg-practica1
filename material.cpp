#include "material.h"
#include <assert.h>
Material::Material(){
    reflecAmb=new GLfloat[4]{0.5,0.5,0,1.0};
    reflecDif=new GLfloat[4]{0.5,0.5,0,1.0};
    reflecEsp=new GLfloat[4]{1.0,1.0,1.0,1.0};
    brillo=15.0;
}
Material::Material(const Material &mat){
    this->reflecAmb=new GLfloat[4];
    this->reflecDif=new GLfloat[4];
    this->reflecEsp=new GLfloat[4];
    for(int i=0;i<4;i++){
        this->reflecAmb[i]=mat.reflecAmb[i];
        this->reflecDif[i]=mat.reflecDif[i];
        this->reflecEsp[i]=mat.reflecEsp[i];
    }
    this->brillo=mat.brillo;
}
/*************************
*Reflectividad ambiental *
*************************/
void Material::set_ambiental(float r, float g, float b)
{
    reflecAmb[0]=r; reflecAmb[1]=g; reflecAmb[2]=b;
}
void Material::set_ambiental(float *c)
{
    reflecAmb[0]=c[0]; reflecAmb[1]=c[1]; reflecAmb[2]=c[2];
}

/*************************
*Reflectividad especular *
*************************/
void Material::set_especular(float r, float g, float b)
{
    reflecEsp[0]=r; reflecEsp[1]=g; reflecEsp[2]=b;
}
void Material::set_especular(float *c)
{
    reflecAmb[0]=c[0]; reflecAmb[1]=c[1]; reflecAmb[2]=c[2];
}

 /*************************
 *Reflectividad difusa    *
 *************************/
void Material::set_difusa(float r, float g, float b)
{
    reflecDif[0]=r; reflecDif[1]=g; reflecDif[2]=b;
}
void Material::set_difusa(float *c)
{
    reflecAmb[0]=c[0]; reflecAmb[1]=c[1]; reflecAmb[2]=c[2];
}

void Material::set_brillo(float b)
{
    brillo=b;
}
/*************************
*Activar material        *
*************************/
void Material::activar()
{
    glPushAttrib(GL_LIGHTING_BIT);
    glMaterialfv(GL_FRONT,GL_AMBIENT,(GLfloat *) reflecAmb);
    glMaterialfv(GL_FRONT,GL_DIFFUSE,(GLfloat *) reflecDif);
    glMaterialfv(GL_FRONT,GL_SPECULAR,(GLfloat *) reflecEsp);
    glMaterialf(GL_FRONT,GL_SHININESS,brillo);

}

void Material::desactivar()
{
    glPopAttrib();
    /*
    GLfloat amb[4]={0.2, 0.2, 0.2, 1.0};
    GLfloat dif[4] ={ 0.8, 0.8, 0.8, 1.0};
    GLfloat esp[4] = {0.0, 0.0, 0.0, 1.0};
    GLfloat shi = 0;

    glMaterialfv(GL_FRONT,GL_AMBIENT,amb);
    glMaterialfv(GL_FRONT,GL_DIFFUSE,dif);
    glMaterialfv(GL_FRONT,GL_SPECULAR,esp);
    glMaterialf(GL_FRONT,GL_SHININESS,shi);
*/
}
