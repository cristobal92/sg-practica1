#ifndef CARRUSEL_H
#define CARRUSEL_H
#include "objeto3dply.h"
#include "objeto3drev.h"
#include "caballito.h"
class Carrusel
{
private:
    Caballito caballo;
    float elevCaballos[3],rot;
    int nCaballos,caballoDetenido, animacionActiva;
    Objeto3DRev base1,base2,centro,techo;
public:
    Carrusel();
    void dibujar();
    void animacion();
    void detener(int id);
};

#endif // CARRUSEL_H
