#ifndef TEXTURA_H
#define TEXTURA_H
#include "jpg/jpg_imagen.hpp"
#include <GL/glut.h>
class Textura
{
private:
    jpg::Imagen * pimg;
    unsigned char *texels;
    unsigned textX,textY;
    GLuint Id;
public:
    Textura(char *ruta=0);
    void set_textura(char *ruta);
    void activar();
};

#endif // TEXTURA_H
