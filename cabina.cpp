#include "cabina.h"
#include <math.h>
Cabina::Cabina()
{
    //Creacion techo(cupula)
    int nPuntos=20;
    float te[nPuntos*6];
    float rad=M_PI*0.5/(float)(nPuntos-1);
    //Perfil de la cupula interior
    for(int i=0,j=nPuntos-1;i<nPuntos*3;i+=3,j--){
        te[i]=cos(rad*j)*0.9;
        te[i+1]=sin(rad*j)*0.9;
        te[i+2]=0.0;
    }
    //perfil de la cupula exterior
    for(int i=nPuntos*3,j=0;i<nPuntos*6;i+=3,j++){
        te[i]=cos(rad*j);
        te[i+1]=sin(rad*j);
        te[i+2]=0.0;
    }
    techo.set(te,nPuntos*6);
    techo.generarFiguraRevolucion(50);
    techo.rutaTextura("../sg-practica1/texturas/colores.jpg");
    //Parte inferior de la cabina
    float parte[]={0.0,0.0,0.0,
                   1.0,0.0,0.0,
                   1.0,1.0,0.0,
                   0.9,1.0,0.0,
                   0.9,0.5,0.0,
                   0.6,0.5,0.0,
                   0.6,0.1,0.0,
                   0.0,0.1,0.0};
    parteInf.set(parte,sizeof(parte)/sizeof(float));
    parteInf.generarFiguraRevolucion(50);
    parteInf.rutaTextura("../sg-practica1/texturas/colores.jpg");
    //Cilindro
    float cil[]={0.0,0.0,0.0,
                 1.0,0.0,0.0,
                 1.0,1.0,0.0,
                 0.0,1.0,0.0};
    cilindro.set(cil,sizeof(cil)/sizeof(float));
    cilindro.generarFiguraRevolucion(20);
    Material azul;
    azul.set_ambiental(0.0,0.1,0.2);
    azul.set_difusa(0.0,0.50980392,0.50980392);
    azul.set_especular(0.50196078,0.50196078,0.50196078);
    azul.set_brillo(0);
    cilindro.setMaterial(azul);
}

void Cabina::dibujarBarras(){
    int nc=4;
    float rad=2*M_PI/nc;
    glPushMatrix();
    glRotatef(45,0,1,0);
    for(int i=0;i<nc;i++){
        glPushMatrix();
        glTranslatef(cos(rad*i)*0.95,1,sin(rad*i)*0.95);
        glScalef(0.04,1,0.04);
        cilindro.dibujar();
        glPopMatrix();
    }
    glPopMatrix();
}
void Cabina::dibujarTecho(){
    glPushMatrix();
        //Se dibuja la cupula
        glTranslatef(0,2,0);
        techo.dibujar();
        //Se dibuja el soporte que sostendra la cabina a la noria
        glTranslatef(0,1.2,0);
        glPushMatrix();
            glScalef(0.3,0.3,0.3);
            glRotatef(90,0,0,1);
            cilindro.dibujar();
        glPopMatrix();
        glPushMatrix();
            glScalef(3,0.1,0.1);
            glTranslatef(0.5,0,0);
            glRotatef(90,0,0,1);
            cilindro.dibujar();
        glPopMatrix();
    glPopMatrix();
}
void Cabina::dibujar(){
    glPushMatrix();
        glTranslatef(0,-1.6,0);
        glScalef(0.5,0.5,0.5);
        parteInf.dibujar();
        this->dibujarBarras();
        this->dibujarTecho();
    glPopMatrix();
}
