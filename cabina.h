#ifndef CABINA_H
#define CABINA_H
#include "objeto3drev.h"
class Cabina
{
private:
    Objeto3DRev techo,parteInf,cilindro;
    void dibujarBarras();
    void dibujarTecho();
public:
    Cabina();
    void dibujar();
    void animacion();
};

#endif // CABINA_H
