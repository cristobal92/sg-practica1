//**************************************************************************
// Práctica SG
//
// Cristobal Molina Mallorquin y Rogelio Garcia Peña
//
//
//**************************************************************************

#ifndef OBJETO3DPLY_H
#define OBJETO3DPLY_H

#include <GL/gl.h>
#include <vector>
#include "vertex.h"
#include "file_ply_stl.hpp"
#include "objeto3d.h"

class Objeto3DPLY : virtual public Objeto3D
{

    public:
        Objeto3DPLY();
        Objeto3DPLY(char *ruta);
        void setPLY(char *ruta);
        void generarNormales();
        void dibujarAjedrez();
        void dibujar();
};

#endif // OBJETO3DPLY_H
