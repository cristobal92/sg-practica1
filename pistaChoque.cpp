#include "pistaChoque.h"
#include <math.h>
pistaChoque::pistaChoque()
{
    //La clase controla la posicion y rotacion de cada coche ademas de las colisiones
    cochesDetenidos.resize(4);
    posCoches.push_back(_vertex3f(0,0,0));
    posCoches.push_back(_vertex3f(3,0,4));
    posCoches.push_back(_vertex3f(3,0,-4));
    posCoches.push_back(_vertex3f(-3,0,4));

    rotCoches.push_back(0);
    rotCoches.push_back(30);
    rotCoches.push_back(180);
    rotCoches.push_back(200);
    radioColision=1.7;
    //Materiales de los objetos que componen la pista
    Material rojo;
    rojo.set_difusa(1,0,0);
    rojo.set_ambiental(0.5, 0, 0);
    rojo.set_especular(0.1,0.1,0.1);
    rojo.set_brillo(6);
    Material verde;
    verde.set_difusa(0.6,0.7,0.3);
    verde.set_ambiental(0.6, 0.7, 0.3);
    verde.set_especular(0.1,0,0.1);
    verde.set_brillo(5);
    Material azul;
    azul.set_ambiental(0.0,0.1,0.2);
    azul.set_difusa(0.0,0.50980392,0.50980392);
    azul.set_especular(0.20196078,0.30196078,0.80196078);
    azul.set_brillo(6);
    //Base de la pista
    float b[]={0.0,0.0,0.0,
                5.5,0.0,0.0,
                5.5,0.6,0.0,
                4.5,0.6,0.0,
                4.5,0.0,0.0};
    base.set(b,sizeof(b)/sizeof(float));
    base.generarFiguraRevolucion(4);
    base.setMaterial(rojo);

    float m[]={0.0,0.0,0.0,
                4.5,0.0,0.0,
                4.5,0.2,0.0,
                0.0,0.2,0.0};
    parteMetal.set(m,sizeof(m)/sizeof(float));
    parteMetal.generarFiguraRevolucion(4);
    parteMetal.rutaTextura("../sg-practica1/texturas/metal.jpg");
    //Techo de la pista
    float t[]={0.0,0.0,0.0,
                5.5,0.0,0.0,
                5.5,0.6,0.0,
                0.0,1.8,0.0};
    techo.set(t,sizeof(t)/sizeof(float));
    techo.generarFiguraRevolucion(4);
    techo.setMaterial(verde);
    float ba[]={0.0,0.0,0.0,
                0.5,0.0,0.0,
                0.5,3.7,0.0,
                0.0,3.7,0.0};
    barra.set(ba,sizeof(ba)/sizeof(float));
    barra.generarFiguraRevolucion(4);
    barra.setMaterial(azul);
}
/***********************************************************************************
*Funcion que devuelve true si hay colision con otro coche o los limites de la pista
***********************************************************************************/
bool pistaChoque::colisiona(int c){
    float dist;
    if( (posCoches[c].z+radioColision) >=9 ) return true;
    if( (posCoches[c].z-radioColision) <= -9 ) return true;
    if( (posCoches[c].x+radioColision) >= 5 ) return true;
    if( (posCoches[c].x-radioColision) <= -5 ) return true;

    for(int i=0;i<posCoches.size();i++){
        if(i!=c){
            dist=(posCoches[i]-posCoches[c]).module();
            if(dist<=radioColision)
                return true;
        }
    }
    return false;
}
/*********************************************************************
*Funcion que controla el avanze de los coches
*********************************************************************/
void pistaChoque::avanzar(int i){
    float rad=rotCoches[i]*M_PI/180;
    float X=cos(rad)*0.1;
    float Y=sin(rad)*0.1;
    posCoches[i].z+=X;
    posCoches[i].x+=Y;
    if(colisiona(i)){
        rotCoches[i]+=2;
        posCoches[i].z-=X;
        posCoches[i].x-=Y;
    }

}
/*********************************************************************
*Dibujar en su posicion cada componenete
*********************************************************************/
void pistaChoque::dibujar()
{
    glPushName(5);
    glPushMatrix();
    glScalef(1.5,1,2.5);
    glRotatef(45,0,1,0);
    base.dibujar();
    parteMetal.dibujar();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0,4.3,0);
    glScalef(1.5,1,2.5);
    glRotatef(45,0,1,0);
    techo.dibujar();
    glPopMatrix();


//###########################################################
//
//------------------BARRAS DE LA PISTA-----------------------
//
//###########################################################
    // BARRA 1
    glPushMatrix();
    glTranslatef(5.5,0.6,9.3);
    glRotatef(45,0,1,0);
    barra.dibujar();
    glPopMatrix();

    // BARRA 2
    glPushMatrix();
    glTranslatef(5.5,0.6,-9.3);
    glRotatef(45,0,1,0);
    barra.dibujar();
    glPopMatrix();

    // BARRA 3
    glPushMatrix();
    glTranslatef(-5.5,0.6,9.3);
    glRotatef(45,0,1,0);
    barra.dibujar();
    glPopMatrix();

    // BARRA 4
    glPushMatrix();
    glTranslatef(-5.5,0.6,-9.3);
    glRotatef(45,0,1,0);
    barra.dibujar();
    glPopMatrix();



//###########################################################
//
//------------------COCHES DE LA PISTA-----------------------
//
//###########################################################
    float escala=0.5;
    for(int i=0;i<posCoches.size();i++){
        glLoadName(i);
        glPushMatrix();
        glTranslatef(posCoches[i].x,0.1,posCoches[i].z);
        glRotated(rotCoches[i],0,1,0);
        glScalef(escala,escala,escala);
        coches.dibujar();
        glPopMatrix();
    }
    glPopName();
}
/*********************************************************************
*Funcion que controla la animacion
*********************************************************************/
void pistaChoque::animacion(){
    for(int i=0;i<posCoches.size();i++){
        //Si el cohe esta detenido no se hace nada
        if(!cochesDetenidos[i]){
            avanzar(i);
            //El coche no tiene por que ir en linea recta, cada vez que avanza un poco gira entre 0 y 3 grados
            rotCoches[i]+=rand() % 4;
        }
    }
}
/*********************************************************************
*Funcion que marca o desmarca un coche como detenido
*********************************************************************/
void pistaChoque::detenerCoche(int id){
    if(id>=0 && id<cochesDetenidos.size()){
        cochesDetenidos[id]=(cochesDetenidos[id]+1)%2;
    }
}
