#ifndef CAMARA_H
#define CAMARA_H
#include "GL/glut.h"
#include "vertex.h"
class Camara
{
private:
    _vertex3f eye;
    _vertex3f center;
    int rotX,rotY;
public:
    Camara(float eX=0, float eY=10, float eZ=10, float cX=0, float cY=0, float cZ=0 );
    void moverVista(float cX, float cY, float cZ);
    void moverCamara(float eX,float eY, float eZ);
    void cargarCamara();
    void rotarCamara(int y,int x);
};

#endif // CAMARA_H
