#include "objeto3dply.h"
#include "file_ply_stl.hpp"
#include "file_ply_stl.cc"

Objeto3DPLY::Objeto3DPLY(){}
/*********************************************************
*Constructor con ruta de un ply
*********************************************************/
Objeto3DPLY::Objeto3DPLY(char *ruta){
    vector<double> v;
    vector<unsigned long> c;
    //este lector solo permite un vector de double y otro de unsigned long
    _file_ply::read(ruta,v,c);
    Vertices.resize(v.size()/3);
    Caras.resize(c.size());
    for(unsigned int i=0,j=0;i<v.size();i+=3,j++){
        Vertices[j].x=v[i];
        Vertices[j].y=v[i+1];
        Vertices[j].z=v[i+2];

    }
    for(unsigned int i=0;i<c.size();i++){
        Caras[i]=c[i] ;
    }
    centro();
    generarNormales();
}
/*********************************************************
*Introducir nuevos parametros a partir de un ply
*********************************************************/
void Objeto3DPLY::setPLY(char *ruta){
    vector<double> v;
    vector<unsigned long> c;
    //este lector solo permite un vector de double y otro de unsigned long
    _file_ply::read(ruta,v,c);
    Vertices.resize(v.size()/3);
    Caras.resize(c.size());
    for(unsigned int i=0,j=0;i<v.size();i+=3,j++){
        Vertices[j].x=v[i];
        Vertices[j].y=v[i+1];
        Vertices[j].z=v[i+2];

    }
    for(unsigned int i=0;i<c.size();i++){
        Caras[i]=c[i] ;
    }
    centro();
    generarNormales();
}
/****************************************************************
*Funcion que obtiene las normales de los vertices y de las caras
****************************************************************/
void Objeto3DPLY::generarNormales()
{
    int tam=Caras.size();
    normalesC.resize(tam/3);
    normalesV.resize(Vertices.size());
    //Se generan las normales de las caras
    for(int i=0,j=0;i<tam;i+=3,j++)
    {
        int a=Caras[i], b=Caras[i+1], c=Caras[i+2];
        _vertex3f AB=Vertices[b] - Vertices[a];
        _vertex3f BC=Vertices[c] - Vertices[b];
        _vertex3f ABC=AB.cross_product(BC);
        //ABC.show_values();
        /*El vector normal del triangulo se suma en el indice correspondiente del vector
         *normalesV para realizar el calculo posteriormente de los vectores normales a los vertices
         */
        normalesV[a]+=ABC;
        normalesV[b]+=ABC;
        normalesV[c]+=ABC;
        ABC.normalize();
        normalesC[j]=ABC;
    }
    //Se normalizan los vectores para completar el calculo de las normales de los vertices
    for(unsigned int i=0;i<normalesV.size();i++){
       normalesV[i].normalize();
       //normalesV[i].show_values();
    }
}
/*********************************************************
*Funcion de dibujado para iluminacion
*********************************************************/
void Objeto3DPLY::dibujar(){
    if(material)
        material->activar();
    glPushMatrix();
    glTranslatef(-centroF->x,-centroF->y,-centroF->z);
    glEnableClientState( GL_TEXTURE_COORD_ARRAY );
    glEnableClientState(GL_NORMAL_ARRAY);
    glNormalPointer(GL_FLOAT, 0, &normalesV[0]);

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &Vertices[0]);


    glDrawElements( GL_TRIANGLES, Caras.size(), GL_UNSIGNED_INT,&Caras[0] );

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
    glPopMatrix();
    if(material)
        material->desactivar();
}
