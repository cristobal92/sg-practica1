//**************************************************************************
// Práctica SG
//
// Cristobal Molina Mallorquin y Rogelio Garcia Peña
//
//
//**************************************************************************

#ifndef OBJETO3D_H
#define OBJETO3D_H

#include <GL/gl.h>
#include <vector>
#include "vertex.h"
#include "textura.h"
#include "material.h"
using namespace std;

class Objeto3D {
protected:
    vector<_vertex3f> Vertices;
    vector<unsigned int> Caras;
    vector<_vertex3f> normalesC;
    vector<_vertex3f> normalesV;
    vector<float> Colores;
    //Centro de la figura
    _vertex3f *centroF;
    bool colorGen;
    vector<_vertex2f> coorTextura;
    Textura *textura;
    Material *material;
public:
    void set(float *V, int tamV, int *C=0, int tamC=0);
    virtual void generarNormales()=0;
    void dibujarAjedrez();
    void centro(float *c=0);
    void setMaterial(Material mat);
};
#endif
