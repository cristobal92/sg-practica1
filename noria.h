#ifndef NORIA_H
#define NORIA_H
#include "cabina.h"
#include "objeto3drev.h"
#include "soportenoria.h"
class Noria
{
private:
    Cabina cabina;
    soporteNoria soporte;
    Objeto3DRev anillo,cilindro;
    int nCab;
    void dibujarAnillo();
    float rot;
    float matrizT[16];
    _vertex3f posCabina;
public:
    Noria();
    void dibujar();
    void animacion();
    void getPosCabina(_vertex3f &pos,_vertex3f &vista);
};

#endif // NORIA_H
