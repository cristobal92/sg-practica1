#ifndef MATERIAL_H
#define MATERIAL_H
#include "GL/glut.h"
#include "textura.h"
class Material
{
private:
    GLfloat *reflecAmb;
    GLfloat *reflecDif;
    GLfloat *reflecEsp;
    GLfloat brillo;
public:
    Material();
    Material(const Material &mat);
    void set_ambiental(float r,float g,float b);
    void set_ambiental(float *c);
    void set_difusa(float r,float g,float b);
    void set_difusa(float *c);
    void set_especular(float r,float g,float b);
    void set_especular(float *c);
    void set_brillo(float b);
    void activar();
    void desactivar();
};

#endif // MATERIAL_H
