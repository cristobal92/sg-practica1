#include "coche.h"

coche::coche()
{
    Material azul;
    azul.set_ambiental(0.0,0.1,0.2);
    azul.set_difusa(0.0,0.50980392,0.50980392);
    azul.set_especular(0.20196078,0.30196078,0.80196078);
    azul.set_brillo(0);
    /*
    Material mat;
    mat.set_difusa(0.0,0.50980392,0.50980392);
    mat.set_ambiental(0.0,0.1,0.06);
    mat.set_especular(0.50196078,0.50196078,0.50196078);
    mat.set_brillo(0.12);
    */
    cochesito.setPLY("../sg-practica1/modelos/coche");
    cochesito.setMaterial(azul);
}

void coche::dibujar()
{
    glPushMatrix();
        glTranslatef(0,1.0,0);
        glScalef(0.025,0.025,0.025);
        glRotatef(-90,1,0,0);
        cochesito.dibujar();
    glPopMatrix();
}

